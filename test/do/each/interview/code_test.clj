(ns do.each.interview.code-test
  (:require [clojure.test :refer :all]
            [clojure.java.io :as io]
            [clojure.spec.alpha :as s]
            [do.each.interview.code :as sut]))

(def uuid (java.util.UUID/randomUUID))
(def cwd (-> "" java.io.File. .getAbsolutePath))
(def tmp-dir (str cwd "temp/" uuid))

(defn create-temp-dir-structure [root]
  (io/make-parents (str root "/f")))

(deftest fs-tree
  (testing "directory contents"
    (testing "empty"
      (is (empty? (sut/paths tmp-dir))))
    (create-temp-dir-structure tmp-dir)
    (with-open [w (io/writer (str tmp-dir "/f"))]
      (.write w "")
      (testing "one file"
        (is (= ["f"] (sut/paths tmp-dir)))))))
