;; GNU AGPLv3 (or later at your option)
;; see bottom for more license info.
;; please keep this notice.

;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; NOTE:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; .dir-locals-2.el will be loaded if it exists
;; it can override any of these values.
;; do not check it into version control.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

((clojure-mode
  (cider-clojure-cli-global-options    . "-A:test")
  (cider-default-cljs-repl             . figwheel-main)
  (cider-figwheel-main-default-options . "dev")
  (cider-preferred-build-tool          . clojure-cli)
  (clojure-align-forms-automatically   . 1)
  (clojure-indent-style                . 'always-align)
  (fill-column                         . 72)
  (indent-tabs-mode                    . nil)
  (cider-auto-test-mode                . 1))
 (clojurec-mode
  (cider-clojure-cli-global-options    . "-A:test")
  (cider-default-cljs-repl             . figwheel-main)
  (cider-figwheel-main-default-options . "dev")
  (cider-preferred-build-tool          . clojure-cli)
  (clojure-align-forms-automatically   . 1)
  (clojure-indent-style                . 'always-align)
  (fill-column                         . 72)
  (indent-tabs-mode                    . nil)
  (cider-auto-test-mode                . 1))
 (clojurescript-mode
  (cider-clojure-cli-global-options    . "-A:test")
  (cider-default-cljs-repl             . figwheel-main)
  (cider-figwheel-main-default-options . "dev")
  (cider-preferred-build-tool          . clojure-cli)
  (clojure-align-forms-automatically   . 1)
  (clojure-indent-style                . 'always-align)
  (fill-column                         . 72)
  (indent-tabs-mode                    . nil)
  (cider-auto-test-mode                . 1)))

;; This file is part of do.each.interview.code.

;; do.each.interview.code is free software: you can redistribute it
;; and/or modify it under the terms of the GNU Affero General Public
;; License as published by the Free Software Foundation, either version
;; 3 of the License, or (at your option) any later version.

;; do.each.interview.code is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied warranty
;; of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public
;; License along with do.each.interview.code. If not, see
;; <http://www.gnu.org/licenses/>.
